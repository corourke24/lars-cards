require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get root_path
    assert_response :success
  end

  test "should get cardz" do
    get cardz_path
    assert_response :success
  end

  test "should get faq" do
    get faq_path
    assert_response :success
  end
  
  test "should get how to play" do
    get how_to_play_path
    assert_response :success
  end

end
