Rails.application.routes.draw do
  
  root 'static_pages#home'

  get '/how-to-play',   to: 'static_pages#how_to_play'
  get '/cardz',         to: 'static_pages#cardz'
  get '/board-games',   to: 'static_pages#board_games'
  get '/downloads',     to: 'downloads#downloads'
  
  get 'testerino',      to: 'static_pages#test_cardz'
end