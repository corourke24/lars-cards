class StaticPagesController < ApplicationController
  def home
  end

  def about
  end

  def contact
  end
  
  def modal
    respond_to do |format|
      format.js
    end
  end
end